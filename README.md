# Kawal Diri App API Router

**This service acts as a router that forwards API requests from Kawal Diri app to its respective services**



A NodeJS Express server compiled with [typescript](https://www.typescriptlang.org/). Based on the boilerplate by [Alex Permiakov](https://itnext.io/production-ready-node-js-rest-apis-setup-using-typescript-postgresql-and-redis-a9525871407).


- [System Design](#system-design)
- [Prerequisites](#prerequisites)
- [Getting Started](#getting-started)
- [Folder Structure](#folder-structure)



## System Design

```mermaid
graph LR;
    index.ts-->Router;
    Router-->index.ts;
    Router-->Service;
    Service-->Router;
    Service-->Accessor;
    Service-->OtherService;
    OtherService-->Service;
    Accessor-->Service;
```

**index.ts** : The main server entry point

**Route** : Presentational layer - comprises of api endpoint mapping to the service it needs to call

 - e.g. `/routes/auth.ts`

**Service** : Business logic layer - comprises of the main business logics we have to implement, though for an API Router like this, there's minimum use case for this.

 - e.g.: `/services/auth.ts`

**Accessor** : Data access layer - comprises of data source accessors, though for an API Router like this, there's minimum use case for this, perhaps for caching in the future.

 - e.g.: `/accessors/AuthRedisAccessor.ts`







## Prerequisites

- [NodeJS](https://nodejs.org/en/download/)





## Getting Started

```bash
# Setup
npm install # install package dependencies

# Development
npm run dev # file watch + hot reload

# Production build
npm run build # compile /src to /dist

# Production run
npm run start # run /dist/index.js
```


## Conventions
- Commit messages: please refer to [Conventional Commits](https://www.conventionalcommits.org/)
- ...TBA



## Folder Structure

```
.
├── package.json # Main package manifest
├── src
│   ├── __tests__ # Unit tests
│   ├── accessors # Data access layer
│   │   └── AuthRedisAccessor.ts
│   ├── config # Configurations for different environments
│   ├── index.ts # Main entry point
│   ├── middlewares # ExpressJS server plugins (middleware)
│   │   └── payload.ts # HTTP request & response payload related middlewares
│   ├── routes # HTTP API endpoint mapping to service calls
│   │   └── auth.ts
│   ├── services # Business logic layer
│   │   └── auth.ts
│   ├── types # Type definitions
│   │   ├── middleware.ts
│   │   └── route.ts
│   └── utils # Utility functions
│       ├── middlewareUtil.ts
│       ├── objectUtil.ts
│       └── routeUtil.ts
├── tsconfig.json # Typescript compiler configs
└── package-lock.json # Dependencies version lock
```

