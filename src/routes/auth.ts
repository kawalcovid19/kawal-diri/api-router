import { Route } from '../types/route';
import * as AuthService from '../services/auth';

export const register: Route = {
  path: '/auth/register',
  method: 'post',
  handler: async (req, res) => {
    const result = await AuthService.register();
    res.send(result);
  }
};

export const login: Route = {
  path: '/auth/login',
  method: 'post',
  handler: async (req, res) => {
    const result = await AuthService.login();
    res.send(result);
  }
};

export const logout: Route = {
  path: '/auth/logout',
  method: 'post',
  handler: async (req, res) => {
    const result = await AuthService.logout();
    res.send(result);
  }
};

export const getUserSession: Route = {
  path: '/auth/session',
  method: 'get',
  handler: async (req, res) => {
    const result = await AuthService.getSessionInfo();
    res.send(result);
  }
};

export const getUserInfo: Route = {
  path: '/auth/user',
  method: 'get',
  handler: async (req, res) => {
    const result = await AuthService.getUserInfo();
    res.send(result);
  }
};