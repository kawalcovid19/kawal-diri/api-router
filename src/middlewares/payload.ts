import parser from 'body-parser';
import { Middleware } from '../types/middleware';

export const handleBodyRequestParsing: Middleware = router => {
  router.use(parser.urlencoded({ extended: true }));
  router.use(parser.json());
};