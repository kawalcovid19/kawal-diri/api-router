import http from 'http';
import express from 'express';

import { applyMiddlewares } from './utils/middlewareUtil';
import { applyRoutes } from './utils/routeUtil';
import { flatten } from './utils/objectUtil';

import * as payloadMiddlewares from './middlewares/payload';

import * as authRoutes from './routes/auth';

process.on('uncaughtException', e => {
  console.error(e);
});

process.on('unhandledRejection', e => {
  console.error(e);
});

const router = express();
applyMiddlewares(flatten(payloadMiddlewares), router);
applyRoutes(flatten(authRoutes), router);

const { PORT = 8080 } = process.env;
const server = http.createServer(router);

server.listen(PORT, () =>
  console.log(`Server is running http://localhost:${PORT}...`)
);