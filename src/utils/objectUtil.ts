/**
 * input:
 *   {
 *     foo: 'foo',
 *   },
 *   {
 *     bar: 'bar',
 *   }
 *
 *
 * output: ['foo', 'bar']
 */
export const flatten = (...objects: object[]): any[] => {
  const ret: any[] = [];
  objects.forEach(obj => {
    Object.values(obj).forEach(value => ret.push(value));
  });
  return ret;
};