import { Router } from 'express';
import { Route } from '../types/route';

export const applyRoutes = (routes: Route[], router: Router) => {
  routes.forEach(route => {
    router[route.method](route.path, route.handler);
  });
};