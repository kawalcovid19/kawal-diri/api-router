import { Router } from 'express';
import { Middleware } from '../types/middleware';

export const applyMiddlewares = (middlewares: Middleware[], router: Router) => {
  middlewares.forEach(middleware => middleware(router));
};